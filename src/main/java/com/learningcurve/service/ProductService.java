package com.learningcurve.service;

import com.learningcurve.entity.Product;
import com.learningcurve.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {


    @Autowired
    ProductRepository productRepository;


    public Product addProduct(Product product){
      return  productRepository.save(product);
    }

    public Optional<Product> getProduct(int id){
        return  productRepository.findById(id);
    }
}
