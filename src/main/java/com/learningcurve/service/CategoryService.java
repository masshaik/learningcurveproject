package com.learningcurve.service;

import com.learningcurve.exceptioin.CategoryException;
import com.learningcurve.dto.CategoryDto;
import com.learningcurve.entity.Category;
import com.learningcurve.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {


    @Autowired
    CategoryRepository categoryRepository;


    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }


    public Optional<Category> getCategory(int id) {
        return categoryRepository.findById(id);
    }

    public Category updateCategory(CategoryDto categoryDto, int id) {
        Optional<Category> category = categoryRepository.findById(id);

        if (!category.isPresent()) {
            throw new CategoryException("Invalid Category");
        }

        Category existedCategory = category.get();

        existedCategory.setCategoryName(categoryDto.getCategoryName());
        existedCategory.setDescription(categoryDto.getDescription());

        return categoryRepository.save(existedCategory);
    }

    public void deleteCategory(int id) {
        categoryRepository.deleteById(id);
    }
}
