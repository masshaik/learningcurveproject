package com.learningcurve.controller;

import com.learningcurve.exceptioin.CategoryException;
import com.learningcurve.exceptioin.ProductException;
import com.learningcurve.entity.Category;
import com.learningcurve.entity.Product;
import com.learningcurve.service.CategoryService;
import com.learningcurve.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("product")
public class ProductController {


    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;


    @PostMapping("/save_product/{categoryId}")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product, @PathVariable("categoryId") int id) {

        Optional<Category> optionalCategory = categoryService.getCategory(id);

        if (!optionalCategory.isPresent()) {
            throw new CategoryException("Invalid Category");
        }

        productService.addProduct(product);
        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    @GetMapping("/get_product/{id}")
    public ResponseEntity<Product> getCategory(@PathVariable("id") int id) throws ProductException {
        Optional<Product> optionalProduct = productService.getProduct(id);
        if (!optionalProduct.isPresent()) {
            throw new ProductException("Product Not Found");
        }
        return new ResponseEntity<>(optionalProduct.get(), HttpStatus.OK);
    }


}
