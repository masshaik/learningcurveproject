package com.learningcurve.controller;

import com.learningcurve.exceptioin.CategoryException;
import com.learningcurve.dto.ApiResponse;
import com.learningcurve.dto.CategoryDto;
import com.learningcurve.entity.Category;
import com.learningcurve.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(name = "category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping("/add_category")
    public ResponseEntity<Category> addCategory(@RequestBody Category category) {

        category = categoryService.addCategory(category);

        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }

    @GetMapping("/get_category/{categoryId}")
    public ResponseEntity<Category> getCategory(@PathVariable("categoryId") int id) {
        Optional<Category> optionalCategory = categoryService.getCategory(id);

        if (!optionalCategory.isPresent()) {
            throw new CategoryException("Category Not Present");
        }
        return new ResponseEntity<>(optionalCategory.get(), HttpStatus.OK);
    }


    @PutMapping("/update_category/{id}")
    public ResponseEntity<ApiResponse> updateCategory(@RequestBody CategoryDto categoryDto, @PathVariable("id") int id) {


        categoryService.updateCategory(categoryDto, id);

        ApiResponse apiResponse = new ApiResponse(true, "Updated Successfully");
        return new ResponseEntity<>(apiResponse, HttpStatus.ACCEPTED);

    }

    @DeleteMapping("/delete_category/{id}")
    public ResponseEntity<ApiResponse> deleteCategory(@PathVariable("id") int id) {
        categoryService.deleteCategory(id);
        return new ResponseEntity<>(new ApiResponse(true, "Deleted Successfully"), HttpStatus.NO_CONTENT);
    }


}
