package com.learningcurve.exceptioin;

import com.learningcurve.dto.ApiResponse;
import com.learningcurve.entity.Category;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApplicationExceptions {


    @ExceptionHandler(CategoryException.class)
    public ResponseEntity<ApiResponse> category() {
        return new ResponseEntity(new ApiResponse(false, "Invalid Category"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductException.class)
    public ResponseEntity<ApiResponse> product() {
        return new ResponseEntity(new ApiResponse(false, "Invalid Product"), HttpStatus.BAD_REQUEST);
    }
}
