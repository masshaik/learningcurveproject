package com.learningcurve.exceptioin;

public class CategoryException extends  RuntimeException {

    public CategoryException(String message) {
        super(message);
    }
}
