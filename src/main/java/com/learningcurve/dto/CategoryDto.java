package com.learningcurve.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.sql.DataSourceDefinition;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {


    private String categoryName;
    private String description;

}
