package com.learningcurve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningCurveApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningCurveApplication.class, args);
	}

}
